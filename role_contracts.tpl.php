<?php
/**
 * Template to display role contracts.
 *
 * Fields available:
 * $rid: The role id number of the role. 
 * $roleName: The name of the selected role.
 * $expires: Number of days til expiration.
 * $form: The form for the user to accept.  
 */
?>

<style>.req_red {color: #ff0000;}</style>
<div class='role'>
	<h2>
		<?php //print t('Role '); print $roleName; ?>
		<?php //print "Expires in $expires days."; ?>
	</h2>
	<?php print $form; ?>
</div>

